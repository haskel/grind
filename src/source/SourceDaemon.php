<?php
namespace App\CoreBundle\Console;

use App\CoreBundle\Exceptions\DaemonException;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\StringInput;

class SourceDaemon
{
    /**
     * In that mode daemon will keep the number of workers
     *
     * If a worker is terminated then a new one will be created
     */
    const MODE_KEEP_WORKERS = 'keep-workers';

    /**
     * In that mode daemon will wait for termination of workers
     *
     * When all workers are terminated daemon will terminate also
     */
    const MODE_WAIT_WORKERS = 'wait-workers';

    /**
     * Number of workers to create for daemon
     *
     * @var int
     */
    protected $totalWorkers = 2;

    /**
     * List of active workers
     *
     * @var array|Worker[]
     */
    protected $workers = array();

    /**
     * Daemon mode
     *
     * @var string
     */
    protected $daemonMode = self::MODE_KEEP_WORKERS;

    /**
     * Daemon active status
     *
     * @var bool
     */
    protected $isActive = false;

    /**
     * Path to the daemon pid file
     *
     * @var string
     */
    protected $pidFile = '';

    /**
     * Daemon PID
     *
     * @var null|integer
     */
    protected $daemonPid = null;

    /**
     * Class name of worker
     *
     * @var string
     */
    protected $workerClass = null;

    /**
     * Kernel instance
     *
     * @var null|KernelInterface
     */
    protected $kernel = null;

    /**
     * Input from CLI
     *
     * @var null|InputInterface
     */
    protected $input = null;

    /**
     * Daemon name
     *
     * @var string
     */
    protected $daemonName = '';

    /**
     * Logger instance
     *
     * @var \Symfony\Component\HttpKernel\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Strategy for workers
     *
     * @var null|DaemonWorkerStrategy
     */
    protected $workerStrategy = null;

    /**
     * Termination start time (when command is received)
     *
     * @var int Microtime
     */
    protected $terminationStartTime = 0;

    /**
     * Time to wait for termination to complete
     *
     * @var int
     */
    protected $terminationWaitTime = 5;

    /**
     * Daemon constructor
     *
     * @param KernelInterface $kernel Kernel instance
     * @param string $name Daemon name
     * @param string $pidFile Path to the pid file
     * @param string $workerClass Worker class to use
     * @param string $mode Mode for daemon, see MODE_XXX constants
     * @param int $totalWorkers Total number of workers to create
     * @param DaemonWorkerStrategy $strategy Specific strategy for worker creation
     */
    public function __construct(KernelInterface $kernel,
                                $name,
                                $pidFile,
                                $workerClass,
                                $mode = self::MODE_KEEP_WORKERS,
                                $totalWorkers = 1,
                                DaemonWorkerStrategy $strategy = null)
    {
        if (!class_exists($workerClass)) {
            throw new DaemonException("Invalid worker class $workerClass");
        }

        $this->kernel         = $kernel;
        $this->logger         = $kernel->getContainer()->get('logger');
        $this->daemonName     = $name;
        $this->pidFile        = $pidFile;
        $this->workerClass    = $workerClass;
        $this->daemonMode     = $mode;
        $this->totalWorkers   = $totalWorkers;
        $this->workerStrategy = $strategy;
        $signalHandler        = $this->getSignalHandler();

        $this->logger->info("Preparing to start the daemon {$this->daemonName}");
        $this->daemonize();

        pcntl_signal(SIGTERM, $signalHandler);
        pcntl_signal(SIGCHLD, $signalHandler);
        pcntl_signal(SIGALRM, $signalHandler);
        pcntl_signal(SIGHUP,  $signalHandler);
        pcntl_signal(SIGTSTP, SIG_IGN);
        pcntl_signal(SIGTTOU, SIG_IGN);
        pcntl_signal(SIGTTIN, SIG_IGN);
    }

    /**
     * Returns daemon name
     *
     * @return string
     */
    public function getName()
    {
        return $this->daemonName;
    }

    /**
     * Start the daemon
     *
     * @param InputInterface $input Command-line input for daemon
     *
     * @return void
     */
    public function start(InputInterface $input)
    {
        $this->logger->info("Starting daemon {$this->daemonName} with pid: {$this->daemonPid}");

        $this->input = $input;
        $this->acquirePid($this->pidFile);
        $this->isActive = true;
        $this->runMainLoop();
        unlink($this->pidFile);
    }

    /**
     * Try to terminate daemon by pid file
     *
     * @param string $pidFile Path to the pid file
     * @param string $daemonName Daemon name
     *
     * @throws \App\CoreBundle\Exceptions\DaemonException If cannot send signal to the daemon
     * @throws \App\CoreBundle\Exceptions\DaemonException If pid file is absent or isn't readable
     *
     * @return void
     */
    public static function terminate($pidFile, $daemonName)
    {
        if (is_readable($pidFile)) {
            $pid = (int) file_get_contents($pidFile);

            if ($pid > 0 && posix_kill($pid, SIGTERM)) {
                $lastError = posix_get_last_error();
                if ($lastError) {
                    throw new DaemonException("Termination of {$daemonName} is failed. " . posix_strerror($lastError));
                } else {
                    return;
                }
            }
        }

        throw new DaemonException("Daemon {$daemonName} is not running or no access to the pid file {$pidFile}");
    }

    /**
     * Returns an information about daemon running status
     *
     * @param string $pidFile Path to the pid file
     *
     * @return bool
     */
    public static function isRunning($pidFile)
    {
        $isRunning = false;
        if (is_readable($pidFile)) {
            $pid = (int) file_get_contents($pidFile);

            if ($pid > 0 && posix_kill($pid, SIG_DFL)) {
                $isRunning = true;
            }
        }
        return $isRunning;
    }

    /**
     * Stop the demon
     *
     * This will send SIGTERM signal to all
     *
     * @param int $signal Termination signal for workers
     *
     * @return void
     */
    public function stop($signal = SIGTERM)
    {
        $activeWorkers = array_keys($this->workers);
        foreach ($activeWorkers as $workerPid) {
            posix_kill($workerPid, $signal);
        }

        $shouldStop = ($signal === SIGTERM);

        if ($shouldStop) {
            $this->terminationStartTime = $this->terminationStartTime ?: microtime(true);

            $timeout = floor($this->terminationWaitTime + $this->terminationStartTime - microtime(true));
            $this->logger->info("Daemon {$this->daemonName} is terminating, timeout is {$timeout}s");
            $this->isActive = false;
        }
    }

    /**
     * Returns PID for daemon
     *
     * @return int|null
     */
    public function getPid()
    {
        return $this->daemonPid;
    }

    /**
     * Return a worker for specified PID
     *
     * @param integer $pid Worker PID
     *
     * @throws DaemonException If worker isn't exist
     * @return Worker
     */
    public function getWorker($pid)
    {
        if (!isset($this->workers[$pid])) {
            throw new DaemonException("Worker with pid $pid is not found");
        }
        return $this->workers[$pid];
    }

    /**
     * Main loop for daemon
     *
     * Creates workers and dispatches all signals
     *
     * @return void
     */
    protected function runMainLoop()
    {
        $remaining = $this->totalWorkers;

        while ($this->isActive || count($this->workers)) {

            // If we started termination then we should be able to force termination after timeout
            if ($this->terminationStartTime) {
                $currentTime = microtime(true);
                if ($currentTime > ($this->terminationStartTime+$this->terminationWaitTime)) {
                    $this->logger->err("Forced termination of daemon {$this->daemonName} after timeout");
                    $this->stop(SIGKILL);
                }
            }

            $workerAction    = $this->workerStrategy->decide();
            $canCreateWorker = count($this->workers) < $this->totalWorkers && $remaining > 0;

            if ($workerAction === DaemonWorkerStrategy::NEED_CREATE && $canCreateWorker) {
                $this->createWorker();
                if ($this->daemonMode === self::MODE_WAIT_WORKERS) {
                    $remaining--;
                }
            }
            if ($workerAction === DaemonWorkerStrategy::NEED_SHUTDOWN && ($workerPid = key($this->workers))) {
                $this->logger->info("Requesting worker $workerPid to terminate");
                posix_kill($workerPid, SIGTERM);
            }

            sleep(60);
            pcntl_signal_dispatch();
        }
    }

    /**
     * Waits for worker's termination
     *
     * Can stop the daemon if it was last worker and mode is MODE_WAIT_WORKERS
     *
     * @return void
     */
    public function waitForWorkers()
    {
        while ($workerPid = pcntl_waitpid(-1, $status, WNOHANG)) {
            if ($workerPid == -1) {
                if ($this->daemonMode == self::MODE_WAIT_WORKERS && $this->isActive) {
                    posix_kill($this->daemonPid, SIGTERM);
                }
                break;

            } else {
                if (pcntl_wifexited($status)) {
                    $exitCode = pcntl_wexitstatus($status);
                    $this->workerStrategy->notifyExited($workerPid, $exitCode);
                    $this->logger->info("Worker $workerPid exited with exit code $exitCode");
                }
                if (pcntl_wifsignaled($status)) {
                    $termSignal = pcntl_wtermsig($status);
                    $this->workerStrategy->notifySignaled($workerPid, $termSignal);
                    $this->logger->err("Worker $workerPid exited by signal $termSignal");
                }
                unset($this->workers[$workerPid]);
            }
        }
    }

    /**
     * Creates a new worker in the separated thread
     *
     * @throws DaemonException
     * @return void
     */
    protected function createWorker()
    {
        $pid = pcntl_fork();
        if ($pid == -1) {
            $errorMessage = 'Can not fork process. ' . posix_strerror(posix_get_last_error());
            throw new DaemonException($errorMessage);
        } else if ($pid) {
            $this->logger->info("Forked new worker with pid $pid");
            $this->workers[$pid] = null;
        } else {
            $workerPid   = getmypid();
            $workerArgs  = $this->input->getOption('args');
            $workerInput = new StringInput($workerArgs);

            $this->setProcessName('worker');

            $worker = new $this->workerClass($this, $this->kernel, $workerInput);
            $this->workers[$workerPid] = $worker;
            $this->workers[$workerPid]->run();
            exit;
        }
    }

    /**
     * Sets process name
     *
     * @param $type
     */
    private function setProcessName($type)
    {
        $processName = sprintf("symfony.daemon %s: %s:%s",
                               (string) $type,
                               $this->kernel->getName(),
                               $this->getName());
        cli_set_process_title($processName);
    }

    /**
     * Returns a handler for signals
     *
     * @throws DaemonException
     * @return \Closure|callable
     */
    protected function getSignalHandler()
    {
        $daemon = $this;
        return function ($signal) use ($daemon) {
            /** @var $daemon Daemon */

            $pid = getmypid();
            $isForDaemon = ($pid == $daemon->getPid());

            switch ($signal) {
                case SIGTERM:
                case SIGHUP:
                    if ($isForDaemon) {
                        $daemon->stop($signal);
                    } else {
                        $worker = $daemon->getWorker($pid);
                        $worker->stop();
                    }
                    break;

                case SIGCHLD:
                case SIGALRM:
                    $daemon->waitForWorkers();
                    break;

                default:
                    throw new DaemonException("Unexpected signal $signal for $pid");
                    break;
            }
        };
    }

    /**
     * Detach from console and become demon
     *
     * @throws DaemonException
     * @return void
     */
    protected function daemonize()
    {
        $this->logger->debug("Exiting from main process");

        $daemonPid = pcntl_fork();
        if ($daemonPid > 0) {
            exit;
        } elseif ($daemonPid == -1) {
            throw new DaemonException(posix_strerror(posix_get_last_error()));
        }

        $this->logger->debug("Isolating daemon process");

        // one more fork to become isolated
        $daemonPid = pcntl_fork();

        if ($daemonPid == -1) {
            throw new DaemonException(posix_strerror(posix_get_last_error()));
        } elseif ($daemonPid > 0) {
            exit;
        }

        $this->setProcessName('master');

        $this->logger->debug("Attempting to become a session leader in the group");

        if (posix_setsid() == -1) {
            $errorMessage = 'Can not become the leader. ' . posix_strerror(posix_get_last_error());
            throw new DaemonException($errorMessage);
        }

        umask(0);
        chdir('/');

        $this->daemonPid = posix_getpid();
    }


    /**
     * Check the status of daemon by looking for PID
     *
     * @param string $pidFile Name of the lock file
     *
     * @throws DaemonException
     * @return void
     */
    protected function acquirePid($pidFile)
    {
        $this->logger->debug("Checking pid file {$pidFile} for existence");
        if (is_readable($pidFile)) {

            if (self::isRunning($pidFile)) {
                $this->logger->err("Daemon {$this->daemonName} already running");
                throw new DaemonException("Daemon {$this->daemonName} already running");
            }

            if (!unlink($pidFile)) {
                $this->logger->err("Unable to delete pid file $pidFile.");
                throw new DaemonException("Unable to delete pid file $pidFile");
            }
        }
        $result = file_put_contents($pidFile, $this->daemonPid, LOCK_EX);
        if ($result === false) {
            throw new DaemonException("Unable to write pid into the file $pidFile");
        }
    }
}